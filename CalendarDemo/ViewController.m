//
//  ViewController.m
//  CalendarDemo
//
//  Created by Rob van der Veer on 14/6/13.
//  Copyright (c) 2013 Rob van der Veer. All rights reserved.
//

#import "ViewController.h"


@interface ViewController ()

@end

@implementation ViewController
bool selectStart;

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

-(void)viewDidAppear:(BOOL)animated
{
    GSVisualPreferences *preferences = [[GSVisualPreferences alloc] init];
    preferences.fillColor = [UIColor clearColor];
    preferences.textColor = [UIColor whiteColor];
    preferences.activeFillColor = [UIColor colorWithWhite:1.0 alpha:0.5];
    preferences.borderColor = [UIColor blackColor];
//  preferences.font = [UIFont fontWithName:@"HelveticaNeue-CondensedBold" size:30.0];
    
    self.calendar = [[GSDatePicker alloc] initWithFrame:self.view.bounds];
    self.calendar.appearance = preferences;
    self.calendar.delegate = self;
    [self.view addSubview:self.calendar];
    
    selectStart = true;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)didChangeSelection:(GSDatePicker *)sender newDate:(NSDate *)date
{
    if(selectStart)
    {
        sender.daysView.startDate = date;
        sender.daysView.endDate = date;
        selectStart = false;
    }
    else
    {
        sender.daysView.endDate = date;
        selectStart = true;
    }
}

@end
